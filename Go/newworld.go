package main

import "fmt"

func main() {
	defering()
}

// Functions
func prints() {
	var a int = 12
	b := &a
	*b = 10
	fmt.Println(&a)
	fmt.Println(b)
}

// Pointers
// pointer types use an (*) as a prefix to type pointed to
// *int - a pointer to an integer
// `&` address of operator, `*` derefernce operator
// complex types like structs are automatically deferenced
// Create pointer to objects
// can use the (&) operator if value type already exists
// ms := mystruct{no: 12}
// p := &ms or p := &mystruct{no: 12}
// using the `new` keyword we can create pointers(without field init..)
// Types with internal pointers
// Slices and Maps contain internal pointers, so copies point to same underlying data
func pointer() {
	a := 41
	var b *int = &a
	*b = 12
	fmt.Println(a, *b)
	// using objects
	type MyStruct struct {
		no uint
	}
	// legacy way
	var str *MyStruct
	str = &MyStruct{no: 20}
	(*str).no = 20
	fmt.Println(str)
	fmt.Println((*str).no)
	// go way
	newStr := &MyStruct{no: 12}
	newStr.no = 10
	fmt.Println(newStr.no)
}

// panic aka Exception
// defered statements will be executed after panicking
// function panic will stop the execution of the rest of code
// and return control to the higher function
func exception() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("Something Bad Happened: ", err)
		}
	}()
	panic("raised Exception")
}

// defer keyword
// used to delay execution of a statement until function exists
// run in `LIFO` (last-in,first-out) order
// arguments evaulated at time defer is executed
func defering() {
	// example 1
	fmt.Println("start")
	defer fmt.Println("middle")
	fmt.Println("end")
	// example 2
	a := "start"
	defer fmt.Println(a)
	a = "end"
}

// looping
// var init.. in loop will only stay in the loop
// `continue` to stop the current iteration
// exiting early `break`, `continue`, `labels`
func looping() {
	// standard looping
	// syntax `for initializer; test; incrementer {}`
	for i := 2; i < 10; i++ {
		if i%2 == 0 {
			fmt.Println(i)
		}
	}
	// multiple statement looping
	for i, j := 5, 0; i < 10; i, j = i+1, j+2 {
		fmt.Println(i, j)
	}
	// initializing first statement before loop
	// syntax `for test {}`
	x := 1
	for x <= 5 {
		fmt.Println(x)
		x++
	}
	// inifinite for loop
	// syntax `for {}`
	i := 5
	for {
		if i <= 0 {
			break
		}
		fmt.Println(i)
		i--
	}
	// iterating slices and arrays
	var a string = "Hello!"
	for k, v := range a {
		fmt.Println(k, string(v))
	}
}

// control flow
// logical operation like or/and will be `||` and `&&`
// invering operation is using `!`
// break also works here ;)
func control() {
	if true {
		fmt.Println("Good To Go!")
	}
	// strange method :)
	if value, ok := myName[3]; ok {
		fmt.Println(value)
	} else if true {
		fmt.Println("KeyError")
	} else {
		fmt.Println("NoU!")
	}
	// switch case :|
	// no falling behaviour :)
	// duplicate case no allowed
	// initializor works
	switch i := 2 + 3; i {
	case 1:
		fmt.Println('1')
	default:
		fmt.Println("default")
	}
	// ususal case
	switch 1 {
	case 1:
		fmt.Println("one")
	case 2:
		fmt.Println("Two")
	case 3, 4, 5:
		fmt.Println("<5")
	default:
		fmt.Println(">5")
	}
	// tagless case
	i := 2
	switch {
	case i <= 2:
		fmt.Println("less that or equal to 2")
		// break is deafult to ! it use
		fallthrough
	case i <= 4:
		fmt.Println("less that equal to 4")
	}
	// type switch
	var j interface{} = 1
	switch j.(type) {
	case int:
		fmt.Println("j is an int")
	case string:
		fmt.Println("j is a string")
	default:
		fmt.Println("j is another type")
	}
}

// struct
// collections of disprate data types that describe a single concept
// keyed by named fields
// normally created as types, but anonymous structs are allowed
// structs are value types
// no inheritance, but can use composition via embedding
// tags can be added to stuct fields to describe field
func structure() {
	type Series struct {
		number     uint16
		heroName   string
		companions []string
	}
	// way 1
	flash := Series{
		number:   2,
		heroName: "Barry Allen",
		companions: []string{
			"Harrison Wells",
			"Cisco Ramon",
			"Catlin Snow",
		},
	}
	// way 2
	arrow := Series{
		1,
		"Oliver Queen",
		[]string{
			"Felicity Smoak",
			"John Diggle",
			"Speedy",
		},
	}

	fmt.Println(flash.heroName)
	fmt.Println(flash.companions[2])
	fmt.Println(arrow)
	// anynoumous struct
	lot := struct {
		seasons int
		name    string
	}{seasons: 5, name: "Legends Of Tommorow"}
	fmt.Println(lot.seasons, lot.name)
	// embeding aka inherting("In A Way")
	type cwu struct {
		totalSeasons int
		rating       float32
	}
	// here we are embedding cwu to arrowverse
	type ArrowVerse struct {
		cwu
		number     uint16
		heroName   string
		companions []string
	}
	Legends := ArrowVerse{
		cwu: cwu{
			totalSeasons: 5,
			rating:       7.9,
		},
		number:   4,
		heroName: "Captain Sara Lance",
		companions: []string{
			"Ray Palmer",
			"Mick Rory",
			"Lenord Snart",
		},
	}
	// . works
	Legends.rating = 8.1
	fmt.Println(Legends)
}

// map
// collections of value types that are accessed via keys
// created via literals or via make function
// multiple assignments refers to same underlying data
var myName = map[int]string{
	1: "A",
	2: "L",
	3: "E",
	4: "N",
}

func dict() {
	fmt.Println(myName[1])
	// deleting the a value
	delete(myName, 1)
	fmt.Println(myName)
	// adding a value
	myName[1] = "A"
	fmt.Println(myName)
	// investigating existense
	_, ok := myName[3]
	println(ok)
	// len() returns the length of the map
	println(len(myName))
}

// arrays
func arrayed() {
	var array1 [2]string
	array2 := [3]int{12, 13, 14}
	array3 := [...]string{"hello", "world", "!"}
	array1[0] = "Hey"
	array1[1] = "Son!"
	fmt.Println(array1, array2, array3)
}

// slice
func sliced() {
	slic := []uint16{1, 2, 3, 4}
	// 1. type, capacity
	makedSlic := make([]uint16, 1)
	makedSlic[0] = 0
	// append is a build in function for slices and arrays
	// append takes one or more arguments
	// first one should be the source of the a|s
	makedSlic = append(makedSlic, 1, 2, 3, 4, 5)
	// concat* slices
	makedSlic = append(makedSlic, slic...)
	// popping elements start - end - middle
	start := slic[1:]         // start
	end := slic[:len(slic)-1] // end
	// beware slic will be changed :/
	middle := append(slic[2:], slic[3:]...) // middle
	fmt.Println(start, end, middle)
	fmt.Println(makedSlic)
	fmt.Printf("%T %v\n", slic, slic)
}
