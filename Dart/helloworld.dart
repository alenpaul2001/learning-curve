import 'dart:io';

void main() {
  String hello = "Hello-World !";
  stdout.writeln(hello);
  // DataType();
  // TypeConversion();
  // Looping();
  // Collections();
  // Functions();
  Class();
}

class Person {
  // Data Members
  int age;
  String name;
  // final cant be changed after declaration
  // final is different for diff objects so its called an object property
  final id;
  // static const is a class property & compile time constant
  // the value will be same for every objects
  static const int main_ID = 10;
  /* Person(String name, [int age = 18]) {  // Default constructor
    this.age = age;
    this.name = name; } */
  Person(this.name, [this.age = 18, this.id = 1]); // Shortcut constructor
  // Person.guest() {
  //   // Named constructor
  //   name = "Guest";
  //   age = 19;
  // }
  // Method
  void show() {
    print("Name is $name and age is $age");
  }
}

void Class() {
  var person1 = Person("Harrison Wells", 20);
  person1.show();
  // var guest = Person.guest();
}

void Functions() {
  int square(int number) {
    return number * number;
  }

  // Inline function
  dynamic inline(var num) => num * num;
  print(square(3) + inline(0));
  // anonymous function
  List some = [8, 7, 6];
  some.forEach((element) {
    print(element);
  });
  // named parameters
  // named parameters has a default value
  // the program will work if you dont give any argument
  // we can also choose the default argument by using assign operator
  int sum({int num1, int num2 = 0}) => num1 + num2;
  print(sum(num2: 3, num1: 2));
  // to make a positional parameter optional put [] in b/w
}

void Collections() {
  // List
  List test = [1, 2, 3, "this"];
  // statically typed List
  List<String> _ = ["A", "B", "C"];
  // constant typed List
  List<int> __ = const [1, 2];
  // assigning a variable to another
  // like in go this will not copy the List
  var ___ = test;
  // creating proper clone
  var _____ = [...test]; // spread operator.
  print(test.length);
  print(test[0]);

  // Set
  var even = {2, 4, 6, 8};
  // set is a collection of unique items
  // any copy of objects will be removed
  var hmm = <String>{}; // hash set
  // or
  Set<String> hallo = {};

  // Map key-value pair
  var gift = {"First": "do", "Second": "die", "Three": "afterlife"};
  print(gift["Three"]);
}

void Looping() {
  // break and continue also works in dart
  // Standard for loop
  for (var i = 1; i <= 10; ++i) {
    print(i);
  }
  // for-in loop
  var example_array = [100, 200, 300];
  for (var i in example_array) {
    print(i);
  }
  // for-each loop
  example_array.forEach((n) => print(n));
  // while loop
  var num = 10;
  while (num > 0) {
    print(num);
    num--;
  }
  do {
    print(num);
    num++;
  } while (num < 10);
}

void CoditionalStatement() {
  int number = 100;
  if (number % 2 == 0) {
    print("Even");
  } else if (number % 3 == 0) {
    print("Odd");
  } else {
    print("Confused Screaming");
  }
  switch (number) {
    case 0:
      print("Even");
      break;
    case 1:
      print("Odd");
      break;
    default:
  }
}

void DataType() {
  /* int
    double
    String  // multiline strings are possible '''this.'''
    bool
    dynamic */

  // 2 type of declarations
  int x = 100;
  var y = 100;
  print(x + y);
  // `dynamic` can change its datatype
  dynamic example = 100;
  example = "test";
  print(example);
  // example of RAW String
  String raw = r"This is a RAW \n String";
  print(raw);
  // string interpulation (like f-string in python)
  print("Interpulation :- $raw");
  // Constants are also found in dart
  const nochange = "execute or die";
  print(nochange);
}

void TypeConversion() {
  // String -> int
  var one = int.parse("1");
  // String -> double
  var two = double.parse("2.1");
  // int -> String
  String newone = 1.toString();
  // double -> String
  String newtwo = 1.2333.toStringAsFixed(2);
  print("$one, $two, $newone, $newtwo");
  // Type test
  if (one is int) {
    print("Integer");
  }
}

void Operators() {
  // Null Aware Operator
  // (?.), (??), (??=)
  // Suppose n doesnt contain Num() object
  // It wont raise Method NotFound Error
  var n = Num();
  var x = null;
  int number, error;
  number = n?.num;
  error = x.num;
  print(number + error);
  // setting default value if null
  number = n?.num ?? 0;
  // assigning values to a null object
  print(number ??= 100);
  // this will set 100 as the value permanently
  // if the object is null.
  // tenrnary :]
  var result = x % 2 == 0 ? "Even" : "Odd";
  print(result);
  // standard
  1 + 2;
  2 - 2;
  3 % 3;
  5 * 5;
  // relational
  1 == 1;
  2 != 3;
  3 >= 2;
  4 <= 5;
  // unary
  var anum = 1;
  anum++;
  ++anum;
  anum += 1;
  anum -= 1;
  print(anum);
  // logical
  anum > anum;
  anum < anum;
  anum != anum;
}

class Num {
  int num = 10;
}
